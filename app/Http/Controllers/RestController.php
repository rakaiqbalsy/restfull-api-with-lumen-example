<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;

class RestController extends Controller
{
    public function index()
    {
        $pegawai = Pegawai::orderBy('created_at', 'DESC')->get();
        return response()->json(['status' => 'success', 'data' => $pegawai], 200);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'nip' => 'required',
            'nama' => 'required',
            'bagian' => 'required'
        ], [
            'nip.required' => 'Harap isi NIP',
            'nama.required' => 'Harap isi Nama',
            'bagian.required' => 'Harap isi bagian'
        ]);

        $pegawai = Pegawai::create([
            'nip' => $request->nip,
            'nama' => $request->nama,
            'bagian' => $request->bagian
        ]);

        return response()->json(['status' => 'success', 'data' => $pegawai], 200);
    }

    public function update(Request $request, $id)
    {
        $pegawai = Pegawai::find($id);

        $pegawai->update($request->all());

        return response()->json(['status' => 'success', 'data' => $pegawai], 200);
    }

    public function delete($id)
    {
        $pegawai = Pegawai::find($id);
        $pegawai->delete();
        return response()->json(['status' => 'success', 'message' => 'Data telah dihapus'], 200);
    }
}

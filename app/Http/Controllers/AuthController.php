<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required',
        ], [
            'email.required' => 'Harap masukan email',
            'email.email' => 'Harap isi email dengan benar user@user.com',
            'password.required' => 'Harap masukan password'
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {
            $user->update(['api_token' => Str::random(30)]);
            return response()->json(['status' => 'success', 'token' => $user->api_token], 200);
        }

        return response()->json(['status' => 'failed', 'message' => 'Email atau password salah'], 400);
    }

    public function register(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'nip' => 'required',
            'bagian' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ], [
            'nip.required' => 'Harap isi NIP',
            'nama.required' => 'Harap isi Nama',
            'bagian.required' => 'Harap isi bagian',
            'email.required' => 'Harap masukan email',
            'email.email' => 'Harap isi email dengan benar user@user.com',
            'email.unique' => 'Email sudah digunakan',
            'password.required' => 'Harap masukan password'
        ]);
        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'api_token' => Str::random(30)
        ]);

        $pegawai = Pegawai::create([
            'user_id' => $user->id,
            'nip' => $request->nip,
            'nama' => $request->nama,
            'bagian' => $request->bagian
        ]);

        $pegawai_id = $pegawai->id;

        return response()->json(['status' => 'success', 'data' => $pegawai], 200);
    }
}

<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    // return $router->app->version();
    return 'Belajar';
});

// Login
$router->post('/rest/auth/login', 'AuthController@login');
// Register
$router->post('/rest/auth/register', 'AuthController@register');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/rest', 'RestController@index');
    $router->post('/rest/create', 'RestController@create');
    $router->put('/rest/update/{id}', 'RestController@update');
    $router->delete('/rest/delete/{id}', 'RestController@delete');
});
